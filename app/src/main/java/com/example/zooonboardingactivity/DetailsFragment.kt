package com.example.zooonboardingactivity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs


import com.example.zooonboardingactivity.databinding.FragmentDetailsBinding



class DetailsFragment: Fragment() {

    private var _binding: FragmentDetailsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )= FragmentDetailsBinding.inflate(inflater, container, false).also { fragmentDetailsBinding ->
        _binding = fragmentDetailsBinding
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.animalName.text = arguments?.getString("name")
        binding.animalFact.text = arguments?.getString("fact")


    }
}



