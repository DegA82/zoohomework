package com.example.zooonboardingactivity.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.zooonboardingactivity.R
import com.example.zooonboardingactivity.ZooActivity
import com.example.zooonboardingactivity.databinding.FragmentFinishedBinding

import com.google.android.material.card.MaterialCardView


class FinishedFragment:Fragment() {

    private var _binding: FragmentFinishedBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) :View? {
        _binding = FragmentFinishedBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnBackToGettingStarted.setOnClickListener {
            findNavController().navigate(R.id.action_finishedFragment_to_gettingStartedFragment)
        }
        binding.btnZooActivity.setOnClickListener {
            val intent = Intent(this@FinishedFragment.context, ZooActivity::class.java)
            intent.putExtra("zooName", "Best Zoo Ever")
            startActivity(intent)
        }


    }



    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun MaterialCardView.setupNavigation(@IdRes id:Int){
        setOnClickListener{it.findNavController().navigate(id)}
    }
}