package com.example.zooonboardingactivity.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zooonboardingactivity.MainActivity
import com.example.zooonboardingactivity.R
import com.example.zooonboardingactivity.databinding.FragmentHelloBinding


class HelloFragment: Fragment() {

    private var _binding: FragmentHelloBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHelloBinding.inflate(inflater, container,false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnGettingStartedFragment.setOnClickListener {
            findNavController().navigate(R.id.action_helloFragment_to_gettingStartedFragment)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}