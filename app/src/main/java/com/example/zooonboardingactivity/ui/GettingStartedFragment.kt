package com.example.zooonboardingactivity.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zooonboardingactivity.R
import com.example.zooonboardingactivity.databinding.FragmentGettingStartedBinding



class GettingStartedFragment:Fragment(){

    private var _binding: FragmentGettingStartedBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentGettingStartedBinding.inflate(inflater, container,false).also {
        _binding = it
    }.root


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnFinishFragment.setOnClickListener {
            findNavController().navigate(R.id.action_gettingStartedFragment_to_finishedFragment)
        }
        binding.btnBackToHello.setOnClickListener {
            findNavController().navigate(R.id.action_gettingStartedFragment_to_helloFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}