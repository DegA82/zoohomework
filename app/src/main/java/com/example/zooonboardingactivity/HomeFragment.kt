package com.example.zooonboardingactivity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zooonboardingactivity.databinding.FragmentHomeBinding


class HomeFragment:Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also { fragmentHomeBinding ->
        _binding = fragmentHomeBinding
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.peacocks.setOnClickListener(){
            val bundle = bundleOf("name" to getString(R.string.peacocks_name), "fact" to getString(R.string.peacocks_fact))
            findNavController().navigate(R.id.action_homeFragment_to_detailsFragment, bundle)
        }

        binding.lions.setOnClickListener(){
            val bundle = bundleOf("name" to getString(R.string.lion_name), "fact" to getString(R.string.lion_fact))
            findNavController().navigate(R.id.action_homeFragment_to_detailsFragment, bundle)
        }

        binding.dolphin.setOnClickListener(){
            val bundle = bundleOf("name" to getString(R.string.dolphin_name), "fact" to getString(R.string.dolphin_fact))
            findNavController().navigate(R.id.action_homeFragment_to_detailsFragment, bundle)
        }

        binding.pandas.setOnClickListener(){
            val bundle = bundleOf("name" to getString(R.string.panda_name), "fact" to getString(R.string.panda_fact))
            findNavController().navigate(R.id.action_homeFragment_to_detailsFragment, bundle)
        }
    }
}


